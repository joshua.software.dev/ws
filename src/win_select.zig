const std = @import("std");
const ws2_32 = std.os.windows.ws2_32;

const timeval_from_ns = @import("common.zig").timeval_from_ns;


pub const WindowsNetworkingSelectError = error{
    NetworkSubsystemFailed,
    SocketNotListening,
    SocketCanceled,
    SocketCallInProgress,
    InvalidSocketInDescriptorSet,
} || std.posix.UnexpectedError;

pub fn wait_until_socket_readable(sock_fd: std.posix.socket_t, timeout_nano_seconds: ?u64) !bool {
    var fd_set = ws2_32.fd_set{ .fd_count = 1, .fd_array = undefined };
    fd_set.fd_array[0] = sock_fd;

    const tv_posix = timeval_from_ns(timeout_nano_seconds);
    const tv: ws2_32.timeval = .{ .tv_sec = tv_posix.tv_sec, .tv_usec = tv_posix.tv_usec, };

    const rc = ws2_32.select(0, &fd_set, null, null, if (timeout_nano_seconds != null) &tv else null);
    if (rc == ws2_32.SOCKET_ERROR) {
        return switch (ws2_32.WSAGetLastError()) {
            .WSANOTINITIALISED => unreachable, // not initialized WSA
            .WSAEFAULT => unreachable,
            .WSAENETDOWN => return error.NetworkSubsystemFailed,
            .WSAEINVAL => return error.SocketNotListening,
            .WSAEINTR => return error.SocketCanceled,
            .WSAEINPROGRESS => return error.SocketCallInProgress,
            .WSAENOTSOCK => return error.InvalidSocketInDescriptorSet,
            else => |err| return std.os.windows.unexpectedWSAError(err),
        };
    }
    if (rc == 0) return false;
    std.debug.assert(rc == 1);
    return true;
}
